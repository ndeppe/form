FORM Mode for Emacs
======================

What is it?
--------
[FORM](http://www.nikhef.nl/~form/) is a symbolic manipulation system
commonly used in high energy physics, though not exclusively.
ND started using FORM for some
calculations in November 2015, and is by no means anywhere beyond
a novice user. Nevertheless, he found not having syntax highlighting
to be a very frustrating experience. To resolve this, he started
FORM mode for [Emacs](http://www.gnu.org/software/emacs/).
It is an Emacs major mode that enables syntax highlighting of FORM scripts.
This mode also works with [Aquamacs](http://aquamacs.org/).

Installation
---------
Download the form.el file and place it in `~/.emacs.d`.
Next add `(load "~/.emacs.d/form.el")` to your
Emacs init file, which is typically `~/.emacs`. You can
manually enter FORM mode by running `M-x` and then
`form-mode`, however it should be loaded automatically
whenever you open an `.frm` file.

Examples
-------
Included is a folder entitled "examples" which includes FORM scripts
for computing a few common scattering cross sections in QED. The
reason I include these is to assist someone in getting to these types
of computations quickly, without learning all the other wonderful things
FORM can do. The two scattering examples are Rutherford and Compton,
though for the Rutherford scattering one can easily change what the
4-vector inner products are to study scattering for different particles
or in different limits. The other example of electron-positron annihilation
producing a muon-anti-muon pair. All of these can be found in
standard quantum field theory references, though it is useful to have
example code to modify for new problems as well as being able to
check steps along the way when learning to do these calculations.

License
---------

See form.el and gpl.txt for license information.

The example codes are distributed under the MIT license contained
in the mitl.txt file.

Known Issues
-----------
1) Currently only comments starting with two or more * are highlighted.
This is primarily because it is the easiest and quickest way to
implement in Emacs, sorry.

Please report any bugs you find. If you can supply a fix, that would be
even better and more appreciated.

Acknowledgements
---------------
ND gratefully acknowledges support from the Natural Sciences and
Engineering Research Council of
Canada([NSERC](http://www.nserc-crsng.gc.ca/)) through the Postgraduate
Scholarships-Doctoral Program. ND is grateful for the resources available
[here](http://www.emacswiki.org/emacs/ModeTutorial) and [here](http://ergoemacs.org/emacs/elisp_syntax_coloring.html), which were
used throughout the development of FORM mode.

ND found
[this tutorial](http://www.nikhef.nl/~form/maindir/documentation/tutorial/tutorial.html)
and the FORM
[reference](http://www.nikhef.nl/~form/maindir/documentation/reference/reference.html)
helpful for writing the FORM example code provided.
