** Compute unpolarized e+e- -> mu+mu- scattering
Off Statistics;
Symbols e,s,t,u,mme,mmu;
Functions eta;
AutoDeclare Vectors p,k;
Indices mu,nu,alpha,beta,sigma,rho;

** Compute the amplitude
L M2 = e^4/(4*s^2)*(g_(1,p1)+mme)*g_(1,mu)*(g_(1,p2)-mme)*g_(1,nu)*d_(mu,alpha)*d_(nu,beta)*(g_(2,p3)+mmu)*g_(2,alpha)*(g_(2,p4)-mmu)*g_(2,beta);

** Do the traces over electron (1) and muon (2)
Trace4,1;
Trace4,2;
.sort

** Replace momenta with Mandelstam variables
id p1.p2 = (s-2*mme^2)/2;
id p3.p4 = (s-2*mmu^2)/2;
id p1.p3 = (mme^2+mmu^2-t)/2;
id p2.p4 = (mme^2+mmu^2-t)/2;
id p1.p4 = (mme^2+mmu^2-u)/2;
id p2.p3 = (mme^2+mmu^2-u)/2;

** Reduce unwanted factors of mass times Mandelstam
id u*mmu^2 = mmu^2*(-t-s+2*mme^2+2*mmu^2);
id u*mme^2 = mme^2*(-t-s+2*mme^2+2*mmu^2);

** Factor out the charge and Mandelstam variable s
contract;
Bracket e,s;

** Note that this gives the answer, 13.68 in Schwartz, just factored
** differently. To see this use the identity
** s+t+u=2m_e^2+2m_mu^2
print;
.end