** For this computation we use equation 13.84 in Schwartz
Symbols e,s,t,u,me,mp,E,v,x,g;
CFunctions cos,sin;
Functions eta,[cos],sin;
Set commuting: cos,sin;
AutoDeclare Vectors p,k;
Indices mu,nu,alpha,beta,sigma,rho;

.sort

** Compute the amplitude
L M2 = e^4/(4*t^2)*(g_(1,p1)+me)*g_(1,nu)*(g_(1,p3)+me)*g_(1,mu)*d_(mu,alpha)*d_(nu,beta)*(g_(2,p4)+mp)*g_(2,alpha)*(g_(2,p2)+mp)*g_(2,beta);
Trace4,1;
Trace4,2;

.sort

** Assuming mp>>me. This gives us the relativistic corrections to
** Rutherford's formula
id p1.p2 = E*mp;
id p2.p3 = E*mp;
id p3.p4 = E*mp;
id p1.p4 = E*mp;
id p2.p4 = mp^2;
id p1.p3 = E^2*(1-v^2*cos(x));
id me^2 = E^2-v^2*E^2;

** Simplify numerator
id cos(x) = 1-2*sin(x/2);

** g is a place holder for v^4*sin(x/2)^4
id t^-2 = 1/(16*E^4*g^4);

** Factor out charge and Mandelstam
Bracket e,t,mp,E,g;

print;
.end