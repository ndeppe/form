** For this computation we use equation 13.84 in Schwartz
Off Statistics;
Symbols e,m;
AutoDeclare Vectors p;
Indices mu,nu,alpha,beta;
.sort

L M2 = e^4/4/4*((g_(1,p3)+m)*(
(g_(1,nu,p1,mu)+g_(1,nu,p2,mu)+m*g_(1,nu,mu))/(p1.p2)-
(g_(1,mu,p2,nu)-g_(1,mu,p4,nu)+m*g_(1,mu,nu))/(p2.p4))*
(g_(1,p2)+m)*(
(g_(1,mu,p1,nu)+g_(1,mu,p2,nu)+m*g_(1,mu,nu))/(p1.p2)-
(g_(1,nu,p2,mu)-g_(1,nu,p4,mu)+m*g_(1,nu,mu))/(p2.p4)));

Trace4,1;

.sort

** From Mandelstam
id p1.p3=p2.p4;
id p2.p3=p1.p4+m^2;
id p3.p4=p1.p2;
id p1.p4 = p1.p1+p1.p2-p2.p4;

** Masses
id p1.p1 = 0;
id p4.p4 = 0;
id p2.p2 = m^2;
id p3.p3 = m^2;

.sort

Bracket e,m;
print;
.end