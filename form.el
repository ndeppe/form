;; Copyright (c) 2015-2016 Nils Deppe

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defvar form-mode-hook nil)
;; I don't want a new map for this. Might use C++ map

;; Autoload for files
(add-to-list 'auto-mode-alist '("\\.frm\\'" . form-mode))

;; FORM syntax table
(defvar form-mode-syntax-table
  (let ((form-mode-syntax-table (make-syntax-table c-mode-syntax-table)))
    (modify-syntax-entry ?_ "w" form-mode-syntax-table)
    ;; (modify-syntax-entry ?. "w" form-mode-syntax-table)
    ;; Change "comment" syntax to two * since one is used for multiplication.
    (modify-syntax-entry ?* ". 12b" form-mode-syntax-table)
    (modify-syntax-entry ?\n "> b" form-mode-syntax-table)
    (modify-syntax-entry ?, "." form-mode-syntax-table)
    form-mode-syntax-table)
    )

;; Keywords in form
(setq form-keywords '("Off" "On" "Statistics" "AutoDeclare" "id" "sum" "Sum" "Multiply" "multiply" "print" "Print" "Format" "format" "Dimension" "dimension" "do" "else" "for" "if" "return" "state" "while" "abrackets" "antibrackets" "also" "antiputinside" "antisymmetrize" "apply" "argexplode" "argimplode" "argument" "auto, autodeclare" "bracket" "cfunctions" "chainin" "chainout" "chisholm" "cleartable" "collect" "commuteinset" "commuting" "compress" "contract" "ctable" "ctensors" "cyclesymmetrize" "deallocatetable" "delete" "denominators" "dimension" "discard" "disorder" "do" "drop" "dropcoefficient" "dropsymbols" "else" "elseif" "endargument" "enddo" "endif" "endinexpression" "endinside" "endrepeat" "endterm" "endwhile" "exit" "extrasymbols" "factarg" "factdollar" "factorize" "fill" "fillexpression" "fixindex" "format" "frompolynomial" "functions" "funpowers" "gfactorized" "global" "goto" "hide" "identify" "idnew" "idold" "if" "ifmatch" "ifnomatch" "index, indices" "inexpression" "inparallel" "inside" "insidefirst" "intohide" "keep" "label" "lfactorized" "load" "local" "makeinteger" "many" "merge" "metric" "moduleoption" "modulus" "multi" "multiply" "ndrop" "nfactorize" "nfunctions" "nhide" "normalize" "notinparallel" "nprint" "nskip" "ntable" "ntensors" "nunfactorize" "nunhide" "nwrite" "off" "on" "once" "only" "polyfun" "polyratfun" "pophide" "print" "print[]" "printtable" "processbucketsize" "propercount" "pushhide" "putinside" "ratio" "rcyclesymmetrize" "redefine" "renumber" "repeat" "replaceloop" "save" "select" "set" "setexitflag" "shuffle" "skip" "sort" "splitarg" "splitfirstarg" "splitlastarg" "stuffle" "sum" "symbols" "symmetrize" "table" "tablebase" "tensors" "term" "testuse" "threadbucketsize" "topolynomial" "totensor" "tovector" "trace4" "tracen" "transform" "tryreplace" "unfactorize" "unhide" "unittrace" "vectors" "while" "write" "Abrackets" "Antibrackets" "Also" "Antiputinside" "Antisymmetrize" "Apply" "Argexplode" "Argimplode" "Argument" "Auto, Autodeclare" "Bracket" "Cfunctions" "Chainin" "Chainout" "Chisholm" "Cleartable" "Collect" "Commuteinset" "Commuting" "Compress" "Contract" "Ctable" "Ctensors" "Cyclesymmetrize" "Deallocatetable" "Delete" "Denominators" "Dimension" "Discard" "Disorder" "Do" "Drop" "Dropcoefficient" "Dropsymbols" "Else" "Elseif" "Endargument" "Enddo" "Endif" "Endinexpression" "Endinside" "Endrepeat" "Endterm" "Endwhile" "Exit" "Extrasymbols" "Factarg" "Factdollar" "Factorize" "Fill" "Fillexpression" "Fixindex" "Format" "Frompolynomial" "Functions" "Funpowers" "Gfactorized" "Global" "Goto" "Hide" "Identify" "Idnew" "Idold" "If" "Ifmatch" "Ifnomatch" "Index, Indices" "Inexpression" "Inparallel" "Inside" "Insidefirst" "Intohide" "Keep" "Label" "Lfactorized" "Load" "Local" "Makeinteger" "Many" "Merge" "Metric" "Moduleoption" "Modulus" "Multi" "Multiply" "Ndrop" "Nfactorize" "Nfunctions" "Nhide" "Normalize" "Notinparallel" "Nprint" "Nskip" "Ntable" "Ntensors" "Nunfactorize" "Nunhide" "Nwrite" "Off" "On" "Once" "Only" "Polyfun" "Polyratfun" "Pophide" "Print" "Print[]" "Printtable" "Processbucketsize" "Propercount" "Pushhide" "Putinside" "Ratio" "Rcyclesymmetrize" "Redefine" "Renumber" "Repeat" "Replaceloop" "Save" "Select" "Set" "Setexitflag" "Shuffle" "Skip" "Sort" "Splitarg" "Splitfirstarg" "Splitlastarg" "Stuffle" "Sum" "Symbols" "Symmetrize" "Table" "Tablebase" "Tensors" "Term" "Testuse" "Threadbucketsize" "Topolynomial" "Totensor" "Tovector" "Trace4" "Tracen" "Transform" "Tryreplace" "Unfactorize" "Unhide" "Unittrace" "Vectors" "While" "Write" "identify") )
;; Types allowed in form
(setq form-types '("S" "Symbol" "symbol" "Symbols" "symbols" "V" "Vector" "vector" "Vectors" "vectors" "I" "Index" "index" "Indices" "indices" "F" "f" "Function" "function" "Functions" "functions" "CF" "cf" "CFunction" "cfunction" "CFunctions" "cfunctions" "Set" "set" "Sets" "sets" "ExtraSymbols" "symmetric" "antisymmetric" "cyclic" "rcyclic" "Local" "L" "Global" "G"))
;; Built in functions in form
(setq form-functions '("abs_" "bernoulli_" "binom_" "conjg_" "content_" "count_" "d_" "dd_" "delta_" "deltap_" "denom_" "distrib_" "div_" "dum_" "dummy_" "dummyten_" "e_" "exp_" "exteuclidean_" "extrasymbol_" "fac_" "factorin_" "farg_" "firstbracket_" "firstterm_" "g5_" "g6_" "g7_" "g_" "gcd_" "gi_" "integer_" "inverse_" "invfac_" "makerational_" "match_" "max_" "maxpowerof_" "min_" "minpowerof_" "mod_" "mod2_" "nargs_" "nterms_" "numfactors_" "pattern_" "poly_" "prime_" "random_" "ranperm_" "rem_" "replace_" "reverse_" "root_" "setfun_" "sig_" "sign_" "sum_" "sump_" "table_" "tbl_" "term_" "termsin_" "termsinbracket_" "theta_" "thetap_"))
;; "Special" keywords in form
(setq form-events '(".store" ".sort" ".end" ".clear"))
;; Constants in form
(setq form-constants '("i_"))

;; Generate regex string for each category of keywords
(setq form-keywords-regexp (regexp-opt form-keywords 'words))
(setq form-type-regexp (regexp-opt form-types 'words))
(setq form-constant-regexp (regexp-opt form-constants 'words))
(setq form-event-regexp (regexp-opt form-events 'words))
(setq form-functions-regexp (regexp-opt form-functions 'words))

;; create the list for font-lock.
;; each category of keyword is given a particular face
(setq form-font-lock-keywords
      `(
        (,form-type-regexp . font-lock-type-face)
        (,form-constant-regexp . font-lock-constant-face)
        (,form-event-regexp . font-lock-builtin-face)
        (,form-functions-regexp . font-lock-function-name-face)
        (,form-keywords-regexp . font-lock-keyword-face)
        ;; note: order above matters, because once colored, that part won't change.
        ;; in general, longer words first
        )
      )

(define-derived-mode form-mode fundamental-mode
  ;; (set (make-local-variable 'font-lock-defaults) '(form-font-lock-keywords))
  (setq font-lock-defaults '(form-font-lock-keywords))
  (set-syntax-table form-mode-syntax-table)
  ;; Inform comment/uncomment to use ** to denote comments
  (setq-local comment-start "** ")
  (setq-local comment-end "")
  (setq mode-name "FORM")
  )

;; clear memory. no longer needed
(setq form-keywords nil)
(setq form-types nil)
(setq form-constants nil)
(setq form-events nil)
(setq form-functions nil)

;; clear memory. no longer needed
(setq form-keywords-regexp nil)
(setq form-types-regexp nil)
(setq form-constants-regexp nil)
(setq form-events-regexp nil)
(setq form-functions-regexp nil)

(provide 'form-mode)
